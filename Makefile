all: configure compile

configure:
	cmake -B build -S .

compile:
	cmake --build build --parallel

clean:
	cmake --build build --target clean
	$(RM) -r build

entr:
	ls *.cxx *.h CMakeLists.txt Makefile | entr -cr make pipe

pipe: all
	build/main
	# sipsak -s sip:dean@localhost || true

