# SIP into something more comfortable...

How to find the library you've forgotten to link against.

```bash
$ find build/lib -name *.a | while read file; do nm -C $file | grep "T pjmedia_sdp_neg_get_neg_local" && echo $file; done
0000000000002470 T pjmedia_sdp_neg_get_neg_local
build/lib/libpjmedia-x86_64-unknown-linux-gnu.a
```

## Command line sip

- [Audio over IP with Dante](https://www.audinate.com/meet-dante/what-is-dante)
- [PJSIP example code](https://docs.pjsip.org/en/2.13/api/pjsip/samples.html)
- [sipcmd2](https://github.com/guisousanunes/sipcmd2)
- sipsak (via apt)
- [sipp](https://sipp.sourceforge.net/doc/reference.html)

