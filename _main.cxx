#include <pjlib-util.h>
#include <pjlib.h>
#include <pjmedia/sdp.h>
#include <pjsip.h>
#include <pjsip_ua.h>

#include <fmt/core.h>
#include <format>
#include <fstream>
#include <numeric>
#include <filesystem>
#include <ranges>
#include <string>
#include <vector>
#include <algorithm>

int main(int argc, char *argv[]) {

  // Initialise the library
  const pj_status_t status = pj_init();

  const std::string success = std::format(
      "PJSIP init {}", status != PJ_SUCCESS ? "failed" : "succeeded");

  fmt::print("{}\n", success);

  // Return if not successful
  if (status != PJ_SUCCESS)
    return 1;

  // Create SIP account
  pj::AccountConfig cfg;

  // Create UDP transport
  pj::TransportConfig tcfg;
  tcfg.port = 5060;
  auto transport = std::make_shared<pj::UdpTransport>(tcfg);



  // Get list of files in directory
  using namespace std::literals::string_view_literals;

  // Create vector of only CSV files
  std::vector<std::string_view> files;
  for (int i = 0; i < argc; ++i)
    if (std::filesystem::path(argv[i]).extension() == ".wav"sv)
      files.push_back(argv[i]);

  // Process each WAV file
  for (auto file : files) {
    auto in = std::ifstream{std::string{file}, std::ios::binary};

    // Create a structure for the WAV header
    struct wav_header {
      unsigned int riff_id;
      unsigned int riff_size;
      unsigned int wave_tag;
      unsigned int format_id;
      unsigned int format_size;
      unsigned int format_tag : 16;
      unsigned int channels : 16;
      unsigned int sample_rate;
      unsigned int bytes_per_second;
      unsigned int block_align : 16;
      unsigned int bit_depth : 16;
      unsigned int data_id;
      unsigned int data_size;
    } header_data;

    // Read WAV header
    in.read(reinterpret_cast<char *>(&header_data), sizeof header_data);

    // Print WAV stats
    fmt::print("Sample rate: {}\n", header_data.sample_rate);
    fmt::print("Bit depth: {}\n", uint32_t{header_data.bit_depth});
    fmt::print("Data size: {}\n", header_data.data_size);
    fmt::print("Channels: {}\n", uint32_t{header_data.channels});

    const auto sparkline = std::string{" ▁▂▃▄▅▆▇█"};

    fmt::print("sparkline: {} levels {}\n", sparkline, std::size(sparkline));

    // Create container for samples
    auto samples =
        std::vector<int16_t>(header_data.data_size / sizeof(int16_t));

    // Read samples
    in.read(reinterpret_cast<char *>(samples.data()), header_data.data_size);

    // Print number of samples
    fmt::print("Number of samples: {}\n", std::size(samples));

    auto chunked_samples = std::vector<double>{};

    // Print samples
    for (auto chunk : samples | std::views::chunk(samples.size() / 20)) {
      const auto av =
          std::accumulate(chunk.begin(), chunk.end(), 0.0) / std::size(chunk);
      // fmt::print("{}\n", std::ranges::fold_left(chunk));
      chunked_samples.push_back(av);
      // fmt::print("{}\n", av);
    }

    const auto &[min, max] = std::ranges::minmax(chunked_samples);

    fmt::print("min: {}\n", min);
    fmt::print("max: {}\n", max);

    auto normalised =
        chunked_samples | std::views::transform([min, max](auto x) -> double {
          return (x - min) / (max - min);
        });

    for (auto x : normalised) {
      // fmt::print("{}\n", x);
      // scale to size of sparkline
      // fmt::print("{}", sparkline[std::size(sparkline) * x]);
      fmt::print("{}", sparkline[9]);
    }

    fmt::print("\ncya\n");
  }
}
