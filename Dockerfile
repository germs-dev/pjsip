FROM ubuntu:devel

RUN apt update
RUN apt install --yes cmake make g++ g++-13 git libfmt-dev

COPY . /usr/src/
WORKDIR /usr/src/
